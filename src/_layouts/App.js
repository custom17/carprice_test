import React from 'react';
import 'normalize.css/normalize.css';
import style from './app.module.sass';
import {connect} from 'react-redux';
import * as actions from '../_actions/dataActions';
import LineCard from '../_components/lineCard/LineCard'
import TitleCard from "../_components/tileCard/TitleCard";
import NewCardForm from "../_components/newCardForm/NewCardForm";
import FilterBtnGroup from "../_components/filterBtnGroup/FilterBtnGroup";

class App extends React.Component {

    state = {isTile: false};

    componentWillMount() {
        this.props.fetchData();
    }

    renderCard() {
        const items = this.props.data || [];

        return items.map((item, i) => {
            return this.state.isTile ? <TitleCard item={item} i={i} key={i}/> : <LineCard item={item} i={i} key={i}/>
        })
    }

    switchDisplay = () => {
        this.setState(prevState => ({
            isTile: !prevState.isTile
        }));
    };

    removeFirstCard = () => {
        this.props.removeFirst();
    };

    removeLastCard = () => {
        this.props.removeLast();
    };

    moveLastCardToTop = () => {
        this.props.moveToTop()
    };

    moveFirstToBottom = () => {
        this.props.moveToBottom()
    };

    submit = values => {
        this.props.addNew(values);
        this.props.clearForm();
    };

    render() {
        return (
            <div className={style.App}>
                <div className={style.BtnGroup}>
                    <button className={style.Btn} onClick={this.moveLastCardToTop}>
                        Добавить в начало
                    </button>
                    <button className={style.Btn} onClick={this.moveFirstToBottom}>
                        Добавить в конец
                    </button>
                    <button className={style.Btn} onClick={this.removeFirstCard}>
                        Удалить первый
                    </button>
                    <button className={style.Btn} onClick={this.removeLastCard}>
                        Удалить последний
                    </button>
                </div>
                <div className={style.Filters}>
                    <h3>Список объектов</h3>
                    <FilterBtnGroup isTile={this.state.isTile} switchDisplay={this.switchDisplay}/>
                </div>
                <div className={style.List}>
                    {this.renderCard()}
                </div>
                <div className={style.Form}>
                    <h3 className={style.FormTitle}>Добавить новый объект</h3>
                    <NewCardForm onSubmit={this.submit}/>
                </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {data: state.data.data};
}

export default connect(mapStateToProps, actions)(App);