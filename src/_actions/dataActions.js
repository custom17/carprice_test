import {FETCH_DATA, REMOVE_FIRST, REMOVE_LAST, MOVE_TO_TOP, MOVE_TO_BOTTOM, ADD_NEW, CLEAR_FORM} from '../_constants';
import data from '../_data/data.json';

export function fetchData() {
    return function (dispatch) {
        dispatch({
            type: FETCH_DATA,
            payload: data.data
        })
    }
}

export function removeFirst() {
    return function (dispatch) {
        dispatch({
            type: REMOVE_FIRST
        })
    }
}

export function removeLast() {
    return function (dispatch) {
        dispatch({
            type: REMOVE_LAST
        })
    }
}

export function moveToTop() {
    return function (dispatch) {
        dispatch({
            type: MOVE_TO_TOP
        })
    }
}

export function moveToBottom() {
    return function (dispatch) {
        dispatch({
            type: MOVE_TO_BOTTOM
        })
    }
}

export function addNew(props) {
    let attributes = props.attributes.split('; ');
    let title = props.title;
    let description = props.description;
    let data = {title, attributes, description};
    return function (dispatch) {
        dispatch({
            type: ADD_NEW,
            payload: data
        });
    }
}

export function clearForm() {
    return function (dispatch) {
        dispatch({
            type: CLEAR_FORM
        })
    }
}