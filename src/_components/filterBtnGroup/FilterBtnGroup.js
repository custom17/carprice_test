import React from 'react';
import style from './filterBtnGroup.module.sass';
import ListIcon from "../icon/ListIcon";
import TileIcon from "../icon/TileIcon";

export default class FilterBtnGroup extends React.Component{
    onClick = () => {
        this.props.switchDisplay()
    };

    render() {
        return (
            <div className={style.FilterGroup}>
                <div>
                    <button className={`${style.Btn} ${this.props.isTile ? null : style.BtnActive}`} onClick={this.onClick} disabled={!this.props.isTile}>
                        <ListIcon/>
                    </button>
                </div>
                <div>
                    <button className={`${style.Btn} ${this.props.isTile ? style.BtnActive : null}`} onClick={this.onClick} disabled={this.props.isTile}>
                        <TileIcon/>
                    </button>
                </div>
            </div>
        )
    }
}