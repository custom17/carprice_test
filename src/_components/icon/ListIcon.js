import React from 'react';

const ListIcon = () => {
    return (
        <svg enableBackground="new 0 0 100 100" height="20px" id="Layer_1" version="1.1"
             viewBox="0 0 500 500" width="20px">
            <g>
                <g>
                    <line fill="none" stroke="#130B7A" strokeLinecap="round"
                          strokeLinejoin="round" strokeMiterlimit="2.6131" strokeWidth="10"
                          x1="55.315" x2="444.685" y1="425" y2="425"/>
                </g>
                <g>
                    <line fill="none" stroke="#130B7A" strokeLinecap="round"
                          strokeLinejoin="round" strokeMiterlimit="2.6131" strokeWidth="10"
                          x1="55.315" x2="444.685" y1="249.981" y2="249.981"/>
                </g>
                <g>
                    <line fill="none" stroke="#130B7A" strokeLinecap="round"
                          strokeLinejoin="round" strokeMiterlimit="2.6131" strokeWidth="10"
                          x1="55.315" x2="444.685" y1="75" y2="75"/>
                </g>
            </g>
        </svg>
    )
};

export default ListIcon;