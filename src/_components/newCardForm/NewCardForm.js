import React from 'react'
import {Field, reduxForm} from 'redux-form'
import style from './newCardForm.module.sass';

const NewCardForm = props => {
    const {handleSubmit, pristine, submitting} = props;
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label className={style.Label}>Заголовок</label>
                <div>
                    <Field
                        className={style.Field}
                        name="title"
                        component="input"
                        type="text"
                    />
                </div>
            </div>
            <div>
                <label className={style.Label}>Добавить пункты через ';'</label>
                <div>
                    <Field
                        className={style.Field}
                        name="attributes"
                        component="input"
                        type="text"
                    />
                </div>
            </div>
            <div>
                <label className={style.Label}>Описание</label>
                <div>
                    <Field
                        className={style.Field}
                        name="description"
                        component="input"
                        type="input"
                    />
                </div>
            </div>
            <div>
                <button className={style.Btn} type="submit" disabled={pristine || submitting}>
                    Добавить
                </button>
            </div>
        </form>
    )
};

export default reduxForm({
    form: 'NewCardForm'
})(NewCardForm)