import React from 'react';
import style from "./lineCard.module.sass";

export default class LineCard extends React.Component{
    render() {
        return (
            <div className={style.Card}>
                <div className={style.CardInfo}>
                    <div className={style.CardNumber}>
                        {this.props.i+1}
                    </div>
                    <div>
                        {this.props.item.title}
                    </div>
                    <ul className={style.CardAttributes}>
                        {
                            this.props.item.attributes.map( (item, i) => {
                                return <li key={i}>{item}</li>
                            })
                        }
                    </ul>
                </div>
                <div className={style.CardDescriptions}>
                    {this.props.item.description}
                </div>
            </div>
        )
    }
}