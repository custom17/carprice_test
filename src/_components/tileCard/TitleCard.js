import React from 'react';
import style from "./titleCard.module.sass";

export default class TitleCard extends React.Component{
    render() {
        return (
            <div className={style.Card}>
                <div className={style.CardInfo}>
                    <div>
                        {this.props.item.title}
                    </div>
                    <div className={style.CardNumber}>
                        {this.props.i + 1}
                    </div>
                </div>
                <ul className={style.CardAttributes}>
                    {
                        this.props.item.attributes.map( (item, i) => {
                            return <li key={i}>{item}</li>
                        })
                    }
                </ul>
                <div>
                    {this.props.item.description}
                </div>
            </div>
        )
    }
}