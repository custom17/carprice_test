import {FETCH_DATA, REMOVE_FIRST, REMOVE_LAST, MOVE_TO_TOP, MOVE_TO_BOTTOM, ADD_NEW} from '../_constants';

export default function (state = {}, action) {
    switch (action.type) {
        case FETCH_DATA:
            state = {
                ...state,
                data: action.payload
            };
            break;

        case REMOVE_FIRST:
            state = {
                ...state,
                data: state.data.slice(1)
            };
            break;

        case REMOVE_LAST:
            state = {
                ...state,
                data: state.data.slice(0, state.data.length - 1)
            };
            break;

        case MOVE_TO_TOP:
            if (state.data.length !== 0) {
                let topData = state.data.slice();
                topData.unshift(state.data[state.data.length - 1]);
                state = {
                    ...state,
                    data: topData
                };
                break;
            }
            break;

        case MOVE_TO_BOTTOM:
            if (state.data.length !== 0) {
                let bottomData = state.data.slice();
                bottomData.push(state.data[0]);
                state = {
                    ...state,
                    data: bottomData
                };
                break;
            }
            break;

        case ADD_NEW:
            let newData = state.data.slice();
            newData.push(action.payload);
            state = {
                ...state,
                data: newData
            };
            break;

        default:
            state = {
                ...state,
                data: state.data
            };
    }
    return state;
}