import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import {CLEAR_FORM} from "../_constants";

import dataReducer from './dataReducer';

const rootReducer = combineReducers({
    form: formReducer.plugin({
        NewCardForm: (state, action) => {
            switch (action.type) {
                case CLEAR_FORM:
                    return undefined;
                default:
                    return state;
            }
        }
    }),
    data: dataReducer,
});

export default rootReducer;