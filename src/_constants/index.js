export const FETCH_DATA = 'FETCH_DATA';
export const REMOVE_FIRST = 'REMOVE_FIRST';
export const REMOVE_LAST = 'REMOVE_LAST';
export const MOVE_TO_TOP = 'MOVE_TO_TOP';
export const MOVE_TO_BOTTOM = 'MOVE_TO_BOTTOM';
export const ADD_NEW = 'ADD_NEW';
export const CLEAR_FORM = 'CLEAR_FORM';